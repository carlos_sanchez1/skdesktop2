import React, { Fragment, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import logo from "./logo.svg";
import "./App.css";
import Realm from "realm";

import { Link } from "react-router-dom";

import TasksScreen from "./Screens/Tasks";

import RoutinesScreen from "./Screens/Routines";
import Community from "./Screens/Routines/Community";

import StudyScreen from "./Screens/Study";
import NotificationsSudyCourses from "./Screens/Study/NotificationsSudy";
import NotificationsSudy from "./Screens/Study/NotificationsSudy/Notifications";
import FlashCardsCourses from "./Screens/Study/FlashCards";
import FlashCards from "./Screens/Study/FlashCards/FlashCards";
import Pomodoros from "./Screens/Study/Pomodoros";
import Exams from "./Screens/Study/Exams";
import Settings from "./Screens/Settings";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import {
  faCheckSquare,
  faCoffee,
  faTimes,
  faAnchor,
  faAmbulance,
  faAngleDoubleDown,
  faAllergies,
  faBabyCarriage,
  faAtom,
  faBan,
  faRunning,
  faUtensils,
  faTrashAlt,
  faCheckCircle,
  faFilter,
  faBell,
  faRedo,
  faLongArrowAltRight,
  faSquareRootAlt,
  faDivide,
  faLanguage,
  faFlask,
  faEllipsisV,
  faEllipsisH,
  faChevronLeft,
  faChevronRight,
  faGraduationCap,
  faMinusCircle,
  faClock,
  faCog,
  faCodeBranch,
  faPlus,
  faCheck,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";

library.add(
  fab,
  faCheckSquare,
  faCoffee,
  faTimes,
  faAnchor,
  faAmbulance,
  faAngleDoubleDown,
  faAllergies,
  faBabyCarriage,
  faAtom,
  faBan,
  faRunning,
  faUtensils,
  faTrashAlt,
  faCheckCircle,
  faFilter,
  faBell,
  faRedo,
  faLongArrowAltRight,
  faSquareRootAlt,
  faDivide,
  faLanguage,
  faFlask,
  faEllipsisV,
  faEllipsisH,
  faChevronLeft,
  faChevronRight,
  faGraduationCap,
  faMinusCircle,
  faClock,
  faCog,
  faCodeBranch,
  faPlus,
  faCheck,
  faTrash
);

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/" component={TasksScreen} />
          <Route path="/routines" component={RoutinesScreen} />
          <Route path="/routines/community" component={Community} />
          <Route path="/study" component={StudyScreen} />
          <Route
            path="/study/notifications-study"
            component={NotificationsSudyCourses}
          />
          <Route
            path="/study/notifications-study/:courseName/:courseId"
            component={NotificationsSudy}
          />
          <Route path="/study/flash-cards" component={FlashCardsCourses} />
          <Route
            path="/study/flash-cards/:courseName/:courseId"
            component={FlashCards}
          />
          <Route path="/study/pomodoros" component={Pomodoros} />
          <Route path="/study/exams" component={Exams} />
          <Route path="/settings" component={Settings} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
