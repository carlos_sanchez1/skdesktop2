import React from 'react';

import Button from '../Button';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const AddButton = ({ onPress, text }) => {
  return (
    <>
      <Button
        onPress={onPress}
        content={
          <div
          // style={{
          //   display: 'flex',
          //   alignItems: 'center',
          // }}
          >
            <text style={{ color: 'white', marginRight: 5 }}>{text}</text>
            <FontAwesomeIcon size="sm" icon="plus" color="white" />
          </div>
        }
        styleBtn={{
          borderRadius: 100,
          height: 40,
          paddingLeft: 33,
          paddingRight: 33,
          backgroundColor: '#0B6DF6',
        }}
      />
    </>
  );
};

export default AddButton;
