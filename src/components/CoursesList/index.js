import React, { useEffect, useState } from 'react';

import { DropdownMenu, MenuItem } from 'react-bootstrap-dropdown-menu';

import AddItemContainer from '../AddItemContainer';
import CustomModal from '../Modal';
import TitleAndIconClose from '../Modal/titleAndIconClose';
import TextAndComponentContainer from '../Modal/textAndComponentContainer';
import Input from '../Input';
import SubmitBottomButtons from '../Modal/submitBottomButtons';
import SwitchSelector from '../SwitchSelector';
import TextLink from '../TextLink';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import axios from 'axios';

import { courseColors, icons } from '../../utils';

import { Link } from 'react-router-dom';

import styles from './CoursesList.module.css';

const CoursesList = ({ goPage }) => {
  const [openModal, setOpenModal] = useState(false);

  const [courseName, setCourseName] = useState('');
  const [courseColorPosition, setCourseColorPosition] = useState(0);
  const [courseIcon, setCourseIcon] = useState('atom');

  const [userCourses, setUserCourses] = useState([]);

  const handleGetUserCourses = async () => {
    const GET_USER_COURSES_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/`;
    try {
      const userRes = await axios.get(GET_USER_COURSES_URI);
      setUserCourses(userRes.data);
      console.log(userRes.data);
      return userRes;
    } catch (error) {
      console.log('ERR GET USER COURSES', error);
    }
  };

  const handleCreateCourse = async (e) => {
    e.preventDefault();

    const jsonSend = {
      name: courseName,
      color_position: courseColorPosition,
      icon: courseIcon,
    };
    const NEW_COURSE_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/newcourse/`;
    try {
      const newCourseRes = await axios.post(NEW_COURSE_URI, jsonSend);
      setUserCourses(newCourseRes.data.new.userCourses);
      console.log(newCourseRes.data.new.userCourses);
      setOpenModal(false);
    } catch (error) {
      console.log('ERR IN CREATE NEW COURSE', error);
    }
  };

  const handleDeleteCourse = async (e, courseId) => {
    e.preventDefault();

    const DELETE_COURSE_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/605e4daaab0af30c7ef0ed90/courses/${courseId}`;
    try {
      const deleteCourseRes = await axios.delete(DELETE_COURSE_URI);
      setUserCourses(deleteCourseRes.data.deleted);
    } catch (error) {
      console.log('ERR TO DELETE COURSE', error);
    }
  };

  useEffect(() => {
    handleGetUserCourses();
  }, []);

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        // backgroundColor: 'red',
        margin: '25px',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          // backgroundColor: 'gray',
        }}
      >
        <AddItemContainer
          itemAreaText="Courses"
          onPressFunction={() => setOpenModal(true)}
          buttonText="New Course"
        />
        <CustomModal
          open={openModal}
          customHeight="300px"
          customTop="20%"
          overlayClick={(value) => setOpenModal(value)}
          content={
            <div
              style={{
                // backgroundColor: 'steelblue',
                height: '100%',
              }}
            >
              <TitleAndIconClose
                modalTitle="Create New Course"
                closeModal={(value) => setOpenModal(value)}
              />
              <div style={{ paddingLeft: 25, paddingRight: 25 }}>
                <TextAndComponentContainer>
                  <text>Name</text>
                  <Input
                    inputValue={courseName}
                    examplePlaceHolder="Ex; Math"
                    inputValueFunction={(e) => setCourseName(e.target.value)}
                    inputType="email"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Color</text>
                  <SwitchSelector
                    customBorder="8px"
                    scroll={true}
                    content={courseColors.map((item) =>
                      item.position === courseColorPosition ? (
                        <div
                          onClick={() => setCourseColorPosition(item.position)}
                          style={{
                            backgroundImage: `linear-gradient(to bottom right, ${
                              courseColors[item.position].color1
                            }, ${courseColors[item.position].color2} 70%)`,
                            display: 'flex',
                            alignItems: 'center',
                            paddingLeft: '39px',
                            paddingRight: '39px',
                            marginLeft: 2,
                            marginRight: 2,
                            paddingTop: '13px',
                            paddingBottom: '14px',
                            borderRadius: '7px',
                            borderStyle: 'solid',
                            borderWidth: 3,
                            borderColor: 'black',
                          }}
                        />
                      ) : (
                        <div
                          onClick={() => setCourseColorPosition(item.position)}
                          style={{
                            backgroundImage: `linear-gradient(to bottom right, ${
                              courseColors[item.position].color1
                            }, ${courseColors[item.position].color2} 70%)`,
                            display: 'flex',
                            alignItems: 'center',
                            paddingLeft: '39px',
                            paddingRight: '39px',
                            marginLeft: 2,
                            marginRight: 2,
                            paddingTop: '16px',
                            paddingBottom: '15px',
                            borderRadius: '7px',
                          }}
                        />
                      )
                    )}
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                    }}
                  >
                    <text>Icon</text>
                    <text style={{ fontSize: 8 }}>optional</text>
                  </div>
                  <SwitchSelector
                    scroll={true}
                    content={icons.map((item) =>
                      item.iconCode === courseIcon ? (
                        <div
                          onClick={() => setCourseIcon(item.iconCode)}
                          style={{
                            backgroundColor: 'green',
                            marginLeft: '2px',
                            marginRight: '2px',
                            paddingLeft: '15px',
                            paddingRight: '15px',
                            paddingTop: '5px',
                            paddingBottom: '5px',
                            borderRadius: '50px',
                          }}
                        >
                          <FontAwesomeIcon size="lg" icon={item.iconCode} />
                        </div>
                      ) : (
                        <div
                          onClick={() => setCourseIcon(item.iconCode)}
                          style={{
                            backgroundColor: 'transparent',
                            marginLeft: '2px',
                            marginRight: '2px',
                            paddingLeft: '15px',
                            paddingRight: '15px',
                            paddingTop: '5px',
                            paddingBottom: '5px',
                            borderRadius: '50px',
                          }}
                        >
                          <FontAwesomeIcon size="lg" icon={item.iconCode} />
                        </div>
                      )
                    )}
                  />
                </TextAndComponentContainer>

                <SubmitBottomButtons
                  cancelFunction={() => setOpenModal(false)}
                  submitFunction={(e) => handleCreateCourse(e)}
                  submitButtonText="Crear"
                  submitBg="lightblue"
                />
              </div>
            </div>
          }
        />
      </div>
      <div className={styles.courses_container}>
        {userCourses.map((item) => (
          <div
            className={styles.course_module}
            style={{
              backgroundImage: `linear-gradient(to bottom right, ${
                courseColors[item.color_position].color1
              }, ${courseColors[item.color_position].color2} 70%)`,
            }}
          >
            <DropdownMenu
              triggerType="icon"
              trigger="glyphicon glyphicon-option-vertical"
              iconColor="white"
              position="center"
            >
              <MenuItem
                text="Delete Course"
                onClick={(e) => handleDeleteCourse(e, item._id)}
              />
            </DropdownMenu>
            <div
              style={{
                display: 'flex',
                justifyContent: 'flex-end',
                padding: '0px 20px',
                cursor: 'pointer',
                height: '30px',
              }}
            >
              {/* <FontAwesomeIcon
                onClick={() => alert('menu')}
                icon="ellipsis-v"
                color="white"
              /> */}
            </div>
            {/* <Link to={`/study/${goPage}/${item.name}/${item._id}`}>
              <div className={styles.course_name_and_icon_course_container}>
                <FontAwesomeIcon
                  icon={item.icon}
                  color="white"
                  style={{ marginBottom: '5px' }}
                />
                <div>{item.name}</div>
              </div>
            </Link> */}
            <TextLink
              goPage={`/study/${goPage}/${item.name}/${item._id}`}
              content={
                <div className={styles.course_name_and_icon_course_container}>
                  <FontAwesomeIcon
                    icon={item.icon}
                    color="white"
                    style={{ marginBottom: '5px' }}
                  />
                  <div>{item.name}</div>
                </div>
              }
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default CoursesList;
