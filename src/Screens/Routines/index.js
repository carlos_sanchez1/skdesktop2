import React, { useEffect, useState } from 'react';

import Container from '../../components/Container';
import Button from '../../components/Button';
import AddItemContainer from '../../components/AddItemContainer';
import CustomModal from '../../components/Modal';
import TitleAndIconClose from '../../components/Modal/titleAndIconClose';
import TextAndComponentContainer from '../../components/Modal/textAndComponentContainer';
import Input from '../../components/Input';
import SubmitBottomButtons from '../../components/Modal/submitBottomButtons';
import SwitchSelector from '../../components/SwitchSelector';
import TextLink from '../../components/TextLink';

import Switch from 'react-switch';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import axios from 'axios';

import { courseColors, icons } from '../../utils';

import styles from './Routines.module.css';

const Routines = () => {
  const [openModal, setOpenModal] = useState(false);

  const [userRoutines, setUserRoutines] = useState([]);

  const [routineNameInput, setRoutineNameInput] = useState('');
  const [routineDescriptionInput, setRoutineDescriptionInput] = useState('');
  const [routineSelectedColorPosition, setRoutineSelectedColorPosition] =
    useState(0);
  const [privateRoutineSwitch, setPrivateRoutineSwitch] = useState(false);

  // const handleGetAllUserRoutines = async () => {
  //   const GET_ALL_USER_ROUTINES_URL = `https://proyect-server.herokuapp.com/api/users/60f59c48ee333c001565e7ca/routines`;
  //   try {
  //     const routinesRes = await axios.get(GET_ALL_USER_ROUTINES_URL);
  //     setUserRoutines(routinesRes.data);
  //     console.log('Routines res', routinesRes.data);
  //   } catch (error) {
  //     console.log('ERR GET ALL USER ROUTINES', error);
  //   }
  // };

  // useEffect(() => {
  //   handleGetAllUserRoutines();
  // }, []);

  const handleCreateRoutine = async (e) => {
    e.preventDefault();

    const jsonSend = {
      routine_name: routineNameInput,
      routine_description: routineDescriptionInput,
      routine_color_position: routineSelectedColorPosition,
      routine_public: privateRoutineSwitch,
    };
    const NEW_ROUTINE_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/newcourse/`;
    try {
      const newRoutineRes = await axios.post(NEW_ROUTINE_URI, jsonSend);
      setUserRoutines(newRoutineRes.data.new);
      console.log(newRoutineRes.data.new);
      setOpenModal(false);
    } catch (error) {
      console.log('ERR IN CREATE NEW ROUTINE', error);
    }
  };

  const handleDeleteRoutine = async (e, courseId) => {
    e.preventDefault();

    const DELETE_ROUTINE_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/${courseId}`;
    try {
      const deleteRoutineRes = await axios.delete(DELETE_ROUTINE_URI);
      setUserRoutines(deleteRoutineRes.data.deleted);
    } catch (error) {
      console.log('ERR TO DELETE ROUTINE', error);
    }
  };

  return (
    <Container navTitle="Routines">
      <div
        style={{
          backgroundColor: 'lightyellow',
        }}
      >
        <TextLink
          goPage="/routines/community"
          content={
            <Button
              content={
                <div>
                  <text>
                    Discover and use the routines of students around the world
                  </text>
                </div>
              }
              styleBtn={{
                // backgroundImage: `url("../../assets/images/skbg.jpg")`,
                height: 80,
                marginTop: 30,
                width: '100%',
              }}
              customClassName={styles.bg}
            />
          }
        />

        <AddItemContainer
          itemAreaText="Your Routines"
          onPressFunction={() => {
            setOpenModal(true);
          }}
          buttonText="New Routine"
        />
        <CustomModal
          open={openModal}
          customHeight="375px"
          customTop="18%"
          overlayClick={(value) => setOpenModal(value)}
          content={
            <div
              style={{
                // backgroundColor: 'steelblue',
                height: '100%',
              }}
            >
              <TitleAndIconClose
                modalTitle="Create New Routine"
                closeModal={(value) => setOpenModal(value)}
              />
              <div style={{ paddingLeft: 25, paddingRight: 25 }}>
                <TextAndComponentContainer>
                  <text>Name</text>
                  <Input
                    inputValue={routineNameInput}
                    examplePlaceHolder="Ex; Weekends"
                    inputValueFunction={(e) =>
                      setRoutineNameInput(e.target.value)
                    }
                    inputType="email"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Description</text>
                  <Input
                    inputValue={routineDescriptionInput}
                    customHeight="45px"
                    examplePlaceHolder="Ex; This Routine is for the best weekends..."
                    inputValueFunction={(e) =>
                      setRoutineDescriptionInput(e.target.value)
                    }
                    inputType="email"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Color</text>
                  <SwitchSelector
                    customBorder="8px"
                    scroll={true}
                    content={courseColors.map((item) =>
                      item.position === routineSelectedColorPosition ? (
                        <div
                          onClick={() =>
                            setRoutineSelectedColorPosition(item.position)
                          }
                          style={{
                            backgroundImage: `linear-gradient(to bottom right, ${
                              courseColors[item.position].color1
                            }, ${courseColors[item.position].color2} 70%)`,
                            display: 'flex',
                            alignItems: 'center',
                            paddingLeft: '39px',
                            paddingRight: '39px',
                            marginLeft: 2,
                            marginRight: 2,
                            paddingTop: '13px',
                            paddingBottom: '14px',
                            borderRadius: '7px',
                            borderStyle: 'solid',
                            borderWidth: 3,
                            borderColor: 'black',
                          }}
                        />
                      ) : (
                        <div
                          onClick={() =>
                            setRoutineSelectedColorPosition(item.position)
                          }
                          style={{
                            backgroundImage: `linear-gradient(to bottom right, ${
                              courseColors[item.position].color1
                            }, ${courseColors[item.position].color2} 70%)`,
                            display: 'flex',
                            alignItems: 'center',
                            paddingLeft: '39px',
                            paddingRight: '39px',
                            marginLeft: 2,
                            marginRight: 2,
                            paddingTop: '16px',
                            paddingBottom: '15px',
                            borderRadius: '7px',
                          }}
                        />
                      )
                    )}
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Private Routine</text>
                  <Switch
                    checkedIcon={false}
                    uncheckedIcon={false}
                    onChange={(xwey, e) =>
                      setPrivateRoutineSwitch(!privateRoutineSwitch)
                    }
                    checked={privateRoutineSwitch}
                    offColor="#EDEBEA"
                  />
                </TextAndComponentContainer>

                <SubmitBottomButtons
                  cancelFunction={() => setOpenModal(false)}
                  submitFunction={(e) => {}}
                  submitButtonText="Crear"
                  submitBg="lightblue"
                />
              </div>
            </div>
          }
        />
      </div>
    </Container>
  );
};

export default Routines;
