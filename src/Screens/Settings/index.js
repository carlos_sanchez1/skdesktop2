import React, { useState, useContext } from 'react';

import SettingsOptionsContext from '../../contexts/SettingsOptionsContext';

import Container from '../../components/Container';
import SettingsContainer from '../../components/SettingsContainer';
import Dropdown from '../../components/DropDown';

import { hourOptions } from '../../utils';

const Settings = () => {
  const { deleteExpired, setDeleteExpired } = useContext(
    SettingsOptionsContext
  );

  const [deleteExpiredTasks, setDeleteExpiredTasks] = useState(true);
  const [soundOnDoneTask, setSoundOnDoneTask] = useState(true);
  const [soundOnDeleteTask, setSoundOnDeleteTask] = useState(true);
  const [animationOnDoneTask, setAnimationOnDoneTask] = useState(true);

  const [selectedHour, setSelectedHour] = useState(0);

  console.log('exired cintext', deleteExpired);
  console.log('exired solo', deleteExpiredTasks);

  return (
    <Container navTitle="Settings" padding={true}>
      <div
        style={{
          // backgroundColor: 'lightgrey',
          padding: 25,
          overflow: 'scroll',
          height: 605,
        }}
      >
        <div>
          <h2>Account</h2>
          <SettingsContainer
            settingTitle="Delete Expired Tasks"
            settingsDescription="se eliminaran todas las tareas vencidas"
            settingSwitch={true}
            switchValue={deleteExpiredTasks}
            swithOnChange={(xwey, e) => {
              setDeleteExpiredTasks(!deleteExpiredTasks);
              setDeleteExpired(!deleteExpired);
            }}
          />
          <SettingsContainer
            settingTitle="Sound on done Task"
            settingsDescription="Reproducir sonido al terminar una tarea"
            settingSwitch={true}
            switchValue={soundOnDoneTask}
            swithOnChange={(xwey, e) => setSoundOnDoneTask(!soundOnDoneTask)}
          />
          <SettingsContainer
            settingTitle="Done Task Sound"
            settingsDescription="Escoge el sonido que quieres escuchar el terminar una tarea"
            settingSwitch={false}
            noSwitchRightContent={
              <Dropdown
                customDisable={soundOnDoneTask ? false : true}
                dropOptions={hourOptions}
                dropValue={(value) => setSelectedHour(value.value)}
                dropPlace="Sound"
              />
            }
          />
          <SettingsContainer
            settingTitle="Sound on Delete Task"
            settingsDescription="Reproducir sonido al eliminar una tarea"
            settingSwitch={true}
            switchValue={soundOnDeleteTask}
            swithOnChange={(xwey, e) =>
              setSoundOnDeleteTask(!soundOnDeleteTask)
            }
          />
          <SettingsContainer
            settingTitle="Animation on done Task"
            settingsDescription="Reproducir Animacion al terminar una tarea"
            settingSwitch={true}
            switchValue={animationOnDoneTask}
            swithOnChange={(xwey, e) =>
              setAnimationOnDoneTask(!animationOnDoneTask)
            }
          />
        </div>
        <div>
          <h2>Tasks</h2>
          <SettingsContainer
            settingTitle="Delete Expired Tasks"
            settingsDescription="se eliminaran todas las tareas vencidas"
            settingSwitch={true}
            switchValue={deleteExpiredTasks}
            swithOnChange={(xwey, e) =>
              setDeleteExpiredTasks(!deleteExpiredTasks)
            }
          />
          <SettingsContainer
            settingTitle="Sound on done Task"
            settingsDescription="Reproducir sonido al terminar una tarea"
            settingSwitch={true}
            switchValue={soundOnDoneTask}
            swithOnChange={(xwey, e) => setSoundOnDoneTask(!soundOnDoneTask)}
          />
          <SettingsContainer
            settingTitle="Done Task Sound"
            settingsDescription="Escoge el sonido que quieres escuchar el terminar una tarea"
            settingSwitch={false}
            noSwitchRightContent={
              <Dropdown
                customDisable={soundOnDoneTask ? false : true}
                dropOptions={hourOptions}
                dropValue={(value) => setSelectedHour(value.value)}
                dropPlace="Sound"
              />
            }
          />
          <SettingsContainer
            settingTitle="Sound on Delete Task"
            settingsDescription="Reproducir sonido al eliminar una tarea"
            settingSwitch={true}
            switchValue={soundOnDeleteTask}
            swithOnChange={(xwey, e) =>
              setSoundOnDeleteTask(!soundOnDeleteTask)
            }
          />
          <SettingsContainer
            settingTitle="Animation on done Task"
            settingsDescription="Reproducir Animacion al terminar una tarea"
            settingSwitch={true}
            switchValue={animationOnDoneTask}
            swithOnChange={(xwey, e) =>
              setAnimationOnDoneTask(!animationOnDoneTask)
            }
          />
        </div>
      </div>
    </Container>
  );
};

export default Settings;
