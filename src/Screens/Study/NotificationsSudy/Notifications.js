import React, { useState, useEffect } from 'react';

import Container from '../../../components/Container';
import AddItemContainer from '../../../components/AddItemContainer';
import Dropdown from '../../../components/DropDown';
import CustomModal from '../../../components/Modal';
import TitleAndIconClose from '../../../components/Modal/titleAndIconClose';
import TextAndComponentContainer from '../../../components/Modal/textAndComponentContainer';
import Input from '../../../components/Input';
import SubmitBottomButtons from '../../../components/Modal/submitBottomButtons';

import { DropdownMenu, MenuItem } from 'react-bootstrap-dropdown-menu';

import Switch from 'react-switch';
import Modal from 'react-modal';

import axios from 'axios';

import { courseColors } from '../../../utils';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { hourOptions, minuteOptions } from '../../../utils';

import styles from './Notifications.module.css';

import { useParams } from 'react-router-dom';

const Notifications = () => {
  let { courseName, courseId } = useParams();

  const [notifications, setNotifications] = useState([]);

  const [courseColor, setCourseColor] = useState(0);

  const [openModal, setOpenModal] = useState(false);

  const [notificationToEditId, setNotificationToEditId] = useState('');

  const [notificationTitle, setNotificationTitle] = useState('');
  const [notificationBody, setNotificationBody] = useState('');
  const [notificationRepeat, setNotificationRepeat] = useState(1);

  const [notificationHour1, setNotificationHour1] = useState(0);
  const [notificationMinute1, setNotificationMinute1] = useState(0);

  const [notificationHour2, setNotificationHour2] = useState(0);
  const [notificationMinute2, setNotificationMinute2] = useState(0);

  const [notificationHour3, setNotificationHour3] = useState(0);
  const [notificationMinute3, setNotificationMinute3] = useState(0);

  useEffect(() => {
    const handleGetUserNotifications = async () => {
      const GET_USER_NOTIFICATIONS_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/${courseId}/`;
      try {
        const userRes = await axios.get(GET_USER_NOTIFICATIONS_URI);
        setNotifications(userRes.data.course.notificationsStudy);
        setCourseColor(userRes.data.course.color_position);
        console.log(userRes.data);
        // return userRes;
      } catch (error) {
        console.log('ERR GET USER NOTIFICATIONS', error);
      }
    };
    handleGetUserNotifications();
  }, [courseId]);

  const handleOnOffNotification = async (e, notiId, active_current_value) => {
    e.preventDefault();

    const ACTIVE_NOTIFICATION_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/${courseId}/notifications-study/${notiId}`;
    const jsonSend = {
      active: !active_current_value,
    };
    try {
      const onOffnotiRes = await axios.patch(ACTIVE_NOTIFICATION_URI, jsonSend);
      setNotifications(onOffnotiRes.data.updated);
      console.log(onOffnotiRes.data.updated);
    } catch (error) {
      console.log('ERR TO ON - OFF NOTI', error);
    }
  };

  const handleCreateNewNotification = async (e) => {
    e.preventDefault();

    const currentDate = new Date();

    const jsonSend = {
      title: notificationTitle,
      body: notificationBody,
      active: true,
      repeat: notificationRepeat,
      repetition_time:
        notificationRepeat === 1
          ? [
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour1,
                soundMinute: notificationMinute1,
              },
            ]
          : notificationRepeat === 2
          ? [
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour1,
                soundMinute: notificationMinute1,
              },
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour2,
                soundMinute: notificationMinute2,
              },
            ]
          : [
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour1,
                soundMinute: notificationMinute1,
              },
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour2,
                soundMinute: notificationMinute2,
              },
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour3,
                soundMinute: notificationMinute3,
              },
            ],
    };
    const NEW_NOTIFICATION_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/${courseId}/notifications-study/new`;
    try {
      const newNotificationRes = await axios.post(
        NEW_NOTIFICATION_URI,
        jsonSend
      );
      setNotifications(newNotificationRes.data.new.userTasks);
      console.log('log en create noti', newNotificationRes.data.new);
      setOpenModal(false);
    } catch (error) {
      console.log('ERR IN CREATE NEW NOTIFICATION', error);
    }
  };

  const handleDeleteNotification = async (e, notiId) => {
    e.preventDefault();

    const DELETE_NOTIFICATION_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/${courseId}/notifications-study/${notiId}`;
    try {
      const deleteNotificationRes = await axios.delete(DELETE_NOTIFICATION_URI);
      setNotifications(deleteNotificationRes.data.deleted);
      console.log(deleteNotificationRes.data.deleted);
    } catch (error) {
      console.log('ERR TO DELETE NOTIFICATION', error);
    }
  };

  const handleEditNotification = async (e, notiId) => {
    e.preventDefault();

    const currentDate = new Date();

    const EDIT_NOTIFICATION_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/605e4daaab0af30c7ef0ed90/courses/${courseId}/notifications-study/${notiId}`;
    const jsonSend = {
      title: notificationTitle,
      body: notificationBody,
      active: true,
      repeat: notificationRepeat,
      repetition_time:
        notificationRepeat === 1
          ? [
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour1,
                soundMinute: notificationMinute1,
              },
            ]
          : notificationRepeat === 2
          ? [
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour1,
                soundMinute: notificationMinute1,
              },
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour2,
                soundMinute: notificationMinute2,
              },
            ]
          : [
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour1,
                soundMinute: notificationMinute1,
              },
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour2,
                soundMinute: notificationMinute2,
              },
              {
                soundYear: currentDate.getFullYear(),
                soundMonth: currentDate.getMonth(),
                soundDay: currentDate.getDate(),
                soundHour: notificationHour3,
                soundMinute: notificationMinute3,
              },
            ],
    };
    try {
      const editNotificationRes = await axios.patch(
        EDIT_NOTIFICATION_URI,
        jsonSend
      );
      setNotifications(editNotificationRes.data.updated);
      console.log('log edit noti', editNotificationRes.data.updated);
      setOpenModal(false);
    } catch (error) {
      console.log('ERR TO EDIT NOTI', error);
    }
  };

  return (
    <Container
      navTitle={`Study - notifications - ${courseName}`}
      returnScreen="/study/notifications-study"
    >
      <div className={styles.container}>
        <AddItemContainer
          itemAreaText="Notifications"
          onPressFunction={() => {
            setOpenModal(true);
            setNotificationTitle('');
            setNotificationBody('');
            setNotificationRepeat(1);
          }}
          buttonText="New Notification"
        />
        <CustomModal
          open={openModal}
          customHeight={
            notificationRepeat === 1
              ? '57%'
              : notificationRepeat === 2
              ? '67%'
              : '77%'
          }
          customTop={
            notificationRepeat === 1
              ? '13%'
              : notificationRepeat === 2
              ? '11%'
              : '7%'
          }
          overlayClick={(value) => setOpenModal(value)}
          content={
            <div
              style={{
                // backgroundColor: 'steelblue',
                height: '100%',
              }}
            >
              <TitleAndIconClose
                modalTitle="Create New Notification"
                closeModal={(value) => setOpenModal(value)}
              />
              <div style={{ paddingLeft: 25, paddingRight: 25 }}>
                <TextAndComponentContainer>
                  <text>Name</text>
                  <Input
                    inputValue={notificationTitle}
                    examplePlaceHolder="Ej. Segunda Guerra mundiañ"
                    inputValueFunction={(e) =>
                      setNotificationTitle(e.target.value)
                    }
                    inputType="text"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Body</text>
                  <Input
                    inputValue={notificationTitle}
                    customHeight="50px"
                    examplePlaceHolder="Ej. informacion de la Segunda Guerra mundiañ"
                    inputValueFunction={(e) =>
                      setNotificationTitle(e.target.value)
                    }
                    inputType="text"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Repeats</text>
                  <Dropdown
                    oneDropLine={true}
                    dropOptions={hourOptions}
                    dropValue={(value) => setNotificationRepeat(value.value)}
                    dropPlace={
                      notificationTitle.length > 0
                        ? notificationRepeat.toString()
                        : 'Repeats'
                    }
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Notification 1</text>
                  <Dropdown
                    dropOptions={hourOptions}
                    dropValue={(value) => setNotificationHour1(value.value)}
                    dropPlace={
                      notificationTitle.length > 0
                        ? notificationHour1.toString()
                        : 'hour 1'
                    }
                  />
                  <Dropdown
                    dropOptions={minuteOptions}
                    dropValue={(value) => setNotificationMinute1(value.value)}
                    dropPlace={
                      notificationTitle.length > 0
                        ? notificationMinute1.toString()
                        : 'minute 1'
                    }
                  />
                </TextAndComponentContainer>

                {notificationRepeat > 1 && notificationRepeat < 4 ? (
                  <TextAndComponentContainer>
                    <text>Notification 2</text>
                    <Dropdown
                      dropOptions={hourOptions}
                      dropValue={(value) => setNotificationHour2(value.value)}
                      dropPlace={
                        notificationTitle.length > 0
                          ? notificationHour2.toString()
                          : 'hour 2'
                      }
                    />
                    <Dropdown
                      dropOptions={minuteOptions}
                      dropValue={(value) => setNotificationMinute2(value.value)}
                      dropPlace={
                        notificationTitle.length > 0
                          ? notificationMinute2.toString()
                          : 'minute 2'
                      }
                    />
                  </TextAndComponentContainer>
                ) : null}

                {notificationRepeat > 2 && notificationRepeat < 4 ? (
                  <TextAndComponentContainer>
                    <text>Notification 3</text>
                    <Dropdown
                      dropOptions={hourOptions}
                      dropValue={(value) => setNotificationHour3(value.value)}
                      dropPlace={
                        notificationTitle.length > 0
                          ? notificationHour3.toString()
                          : 'hour 3'
                      }
                    />
                    <Dropdown
                      dropOptions={minuteOptions}
                      dropValue={(value) => setNotificationMinute3(value.value)}
                      dropPlace={
                        notificationTitle.length > 0
                          ? notificationMinute3.toString()
                          : 'minute 3'
                      }
                    />
                  </TextAndComponentContainer>
                ) : null}

                <SubmitBottomButtons
                  cancelFunction={() => setOpenModal(false)}
                  submitFunction={(e) => handleCreateNewNotification(e)}
                  submitButtonText="Crear"
                  submitBg="lightblue"
                />
              </div>
            </div>
          }
        />
        <div className={styles.notifications_container}>
          {notifications.map((item) => (
            <div
              className={
                item.active ? styles.notification : styles.notification_off
              }
            >
              <div
                style={{
                  backgroundColor: 'transparent',
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  padding: '0px 10px',
                }}
              >
                <div
                  style={{
                    backgroundImage: item.active
                      ? `linear-gradient(to bottom right, ${courseColors[courseColor].color1}, ${courseColors[courseColor].color2} 70%)`
                      : null,
                    backgroundColor: item.active ? null : '#EDEBEA',
                    display: 'inline-block',
                    padding: '4px 12px',
                    borderRadius: '50px',
                    color: 'white',
                  }}
                >
                  <div>{courseName}</div>
                </div>
                <DropdownMenu
                  triggerType="icon"
                  trigger="glyphicon glyphicon-option-horizontal"
                  iconColor={item.active ? 'black' : 'f7f8fc'}
                  position="center"
                >
                  <MenuItem
                    text="Delete"
                    onClick={(e) => handleDeleteNotification(e, item._id)}
                  />
                  <MenuItem
                    text="Edit"
                    onClick={() => {
                      setNotificationToEditId(item._id);
                      setOpenModal(true);
                      setNotificationTitle(item.title);
                      setNotificationBody(item.body);
                      setNotificationRepeat(item.repeat);

                      switch (item.repeat) {
                        case 1:
                          setNotificationHour1(
                            item.repetition_time[0].soundHour
                          );
                          setNotificationMinute1(
                            item.repetition_time[0].soundMinute
                          );
                          break;
                        case 2:
                          setNotificationHour1(
                            item.repetition_time[0].soundHour
                          );
                          setNotificationMinute1(
                            item.repetition_time[0].soundMinute
                          );
                          setNotificationHour2(
                            item.repetition_time[1].soundHour
                          );
                          setNotificationMinute2(
                            item.repetition_time[1].soundMinute
                          );
                          break;
                        default:
                          setNotificationHour1(
                            item.repetition_time[0].soundHour
                          );
                          setNotificationMinute1(
                            item.repetition_time[0].soundMinute
                          );
                          setNotificationHour2(
                            item.repetition_time[1].soundHour
                          );
                          setNotificationMinute2(
                            item.repetition_time[1].soundMinute
                          );
                          setNotificationHour3(
                            item.repetition_time[2].soundHour
                          );
                          setNotificationMinute3(
                            item.repetition_time[2].soundMinute
                          );
                          break;
                      }
                    }}
                  />
                </DropdownMenu>
              </div>
              <div>
                <h5>{item.title}</h5>
                <p>{item.body}</p>
              </div>
              <div
                style={{
                  // backgroundColor: 'lemonchiffon',
                  display: 'flex',
                  justifyContent: 'space-around',
                }}
              >
                <div
                  style={{
                    // backgroundColor: 'lightpink',
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <FontAwesomeIcon icon="redo" style={{ marginRight: '5px' }} />
                  <div>{item.repeat}</div>
                </div>
                <div
                  style={{
                    display: 'flex',
                    // backgroundColor: 'red',
                    width: item.repeat === 3 ? '70%' : '40%',
                    justifyContent: 'space-around',
                  }}
                >
                  {item.repetition_time.map((item) => (
                    <div
                      style={{
                        // backgroundColor: 'lightskyblue',
                        display: 'flex',
                        alignItems: 'center',
                      }}
                    >
                      <FontAwesomeIcon
                        icon="bell"
                        style={{ marginRight: '4px' }}
                      />
                      <div>{item.soundHour}</div>
                      <div>:</div>
                      <div>{item.soundMinute}</div>
                    </div>
                  ))}
                </div>
              </div>
              <Switch
                onChange={(xwey, e) =>
                  handleOnOffNotification(e, item._id, item.active)
                }
                checked={item.active}
                offColor="#EDEBEA"
              />
              {/* <button
                style={{ backgroundColor: item.active ? 'blue' : 'gray' }}
                onClick={(e) =>
                  handleOnOffNotification(e, item._id, item.active)
                }
              >
                activar
              </button> */}
            </div>
          ))}
        </div>
      </div>
    </Container>
  );
};

export default Notifications;
