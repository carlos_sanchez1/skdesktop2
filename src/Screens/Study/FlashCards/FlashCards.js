import React, { useState, useEffect } from 'react';

import Modal from 'react-modal';
import { DropdownMenu, MenuItem } from 'react-bootstrap-dropdown-menu';

import Container from '../../../components/Container';
import AddItemContainer from '../../../components/AddItemContainer';
import CustomModal from '../../../components/Modal';
import TitleAndIconClose from '../../../components/Modal/titleAndIconClose';
import TextAndComponentContainer from '../../../components/Modal/textAndComponentContainer';
import Input from '../../../components/Input';
import SubmitBottomButtons from '../../../components/Modal/submitBottomButtons';

import axios from 'axios';

import { useParams, Link } from 'react-router-dom';

import { courseColors, colorOptions } from '../../../utils';

import './FlashCards.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

Modal.setAppElement('#root');
const FlashCards = () => {
  let { user, courseName, courseId } = useParams();

  const [courseColor, setCourseColor] = useState(0);

  const [openModal, setOpenModal] = useState(false);
  const [isRotated, setIsRotated] = useState(false);
  const [openModalFalshCard, setOpenModalFlashCard] = useState(false);

  const onRotate = () => setIsRotated((rotated) => !rotated);

  const [userFlashCards, setUserFlashCards] = useState([]);

  const [flashCardId, setFlashCardId] = useState('');

  const [flashCardName, setFlashCardName] = useState('');
  const [frontCard, setFrontCard] = useState('');
  const [frontCardImageUrl, setFrontCardImageUrl] = useState('');
  const [backCard, setBackCard] = useState('');
  const [backCardImage, setBackCardImage] = useState('');

  useEffect(() => {
    const handleGetUserFlashCards = async () => {
      const GET_USER_FLASH_CARDS_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/${courseId}/flashcards`;
      try {
        const userRes = await axios.get(GET_USER_FLASH_CARDS_URI);
        setUserFlashCards(userRes.data);
        console.log(userRes.data);
        // setCourseColor(userRes.data.course.color_position);
        return userRes;
      } catch (error) {
        console.log('ERR GET USER FLASH CARDS', error);
      }
    };
    handleGetUserFlashCards();
  }, [courseId]);

  const handleCreateNewFlashCard = async (e) => {
    e.preventDefault();

    const jsonSend = {
      name: flashCardName,
      frontCard: frontCard,
      frontCardImgUrl: frontCardImageUrl,
      backCard: backCard,
      backCardImgUrl: backCardImage,
    };
    const NEW_FALSH_CARD_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/60fb478bee70afa9fe3d485d/courses/${courseId}/flashcards/new`;
    try {
      const newFlashCardRes = await axios.post(NEW_FALSH_CARD_URI, jsonSend);
      setUserFlashCards(newFlashCardRes.data.new);
      console.log('log en create flash card', newFlashCardRes.data);
      setOpenModal(false);
    } catch (error) {
      console.log('ERR IN CREATE NEW FLASH CARD', error);
    }
  };

  const handleDeleteFlashCard = async (e, cardId) => {
    e.preventDefault();

    const DELETE_FLASH_CARD_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/605e4daaab0af30c7ef0ed90/courses/${courseId}/flashcards/${cardId}`;
    try {
      const deleteFlashCardRes = await axios.delete(DELETE_FLASH_CARD_URI);
      setUserFlashCards(deleteFlashCardRes.data.deleted);
      console.log(deleteFlashCardRes.data.deleted);
    } catch (error) {
      console.log('ERR TO DELETE FLASH CARD', error);
    }
  };

  const handleEditFlashCard = async (e, cardId) => {
    e.preventDefault();

    const EDIT_FLASH_CARD_URI = `${process.env.REACT_APP_BACKEND_BASE_URL}/users/605e4daaab0af30c7ef0ed90/courses/${courseId}/flashcards/${cardId}`;
    const jsonSend = {
      name: flashCardName,
      frontCard: frontCard,
      frontCardImgUrl: frontCardImageUrl,
      backCard: backCard,
      backCardImgUrl: backCardImage,
    };

    try {
      const editFlashCardRes = await axios.patch(EDIT_FLASH_CARD_URI, jsonSend);
      setUserFlashCards(editFlashCardRes.data.updated);
      console.log('log edit Flash Card', editFlashCardRes.data);
      setOpenModal(false);
    } catch (error) {
      console.log('ERR TO EDIT FLASH CARD', error);
    }
  };
  return (
    <Container
      navTitle={`Study - Flash Cards - ${courseName}`}
      returnScreen="/study/flash-cards"
    >
      <div className="container">
        <AddItemContainer
          itemAreaText="Flash Cards"
          onPressFunction={() => {
            setFlashCardId('');

            setOpenModal(true);
            setFlashCardName('');
            setFrontCard('');
            setFrontCardImageUrl('');
            setBackCard('');
            setBackCardImage('');
          }}
          buttonText="New Flash Card"
        />
        <CustomModal
          open={openModal}
          customHeight="420px"
          customTop="13%"
          overlayClick={(value) => setOpenModal(value)}
          content={
            <div
              style={{
                // backgroundColor: 'steelblue',
                height: '100%',
              }}
            >
              <TitleAndIconClose
                modalTitle="Create New Flash Card"
                closeModal={(value) => setOpenModal(value)}
              />
              <div style={{ paddingLeft: 25, paddingRight: 25 }}>
                <TextAndComponentContainer>
                  <text>Name</text>
                  <Input
                    inputValue={flashCardName}
                    examplePlaceHolder="Ej. Segunda Guerra Mundial"
                    inputValueFunction={(e) => setFlashCardName(e.target.value)}
                    inputType="text"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Front Card</text>
                  <Input
                    inputValue={frontCard}
                    examplePlaceHolder="Ej. ¿Cuando inicio?"
                    inputValueFunction={(e) => setFrontCard(e.target.value)}
                    inputType="text"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Front Card Image URL</text>
                  <Input
                    inputValue={frontCardImageUrl}
                    examplePlaceHolder="https//www.image.org"
                    inputValueFunction={(e) =>
                      setFrontCardImageUrl(e.target.value)
                    }
                    inputType="text"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Back Card</text>
                  <Input
                    inputValue={backCard}
                    examplePlaceHolder="Ej. 1939"
                    inputValueFunction={(e) => setBackCard(e.target.value)}
                    inputType="text"
                  />
                </TextAndComponentContainer>

                <TextAndComponentContainer>
                  <text>Back Card Image URL</text>
                  <Input
                    inputValue={backCardImage}
                    examplePlaceHolder="https//www.image.org"
                    inputValueFunction={(e) => setBackCardImage(e.target.value)}
                    inputType="text"
                  />
                </TextAndComponentContainer>

                <SubmitBottomButtons
                  cancelFunction={() => setOpenModal(false)}
                  submitFunction={(e) => handleCreateNewFlashCard(e)}
                  submitButtonText="Crear"
                  submitBg="lightblue"
                />
              </div>
            </div>
          }
        />
        <div className="flash_cards_container">
          {userFlashCards.map((item) => (
            <div className="flash_card">
              <div className="flash_card_header">
                <div
                  style={{
                    backgroundImage: `linear-gradient(to bottom right, ${courseColors[courseColor].color1}, ${courseColors[courseColor].color2} 70%)`,
                    display: 'inline-block',
                    padding: '4px 12px',
                    borderRadius: '50px',
                    color: 'white',
                  }}
                >
                  {courseName}
                </div>
                <DropdownMenu
                  triggerType="icon"
                  trigger="glyphicon glyphicon-option-horizontal"
                  iconColor="black"
                  position="center"
                >
                  <MenuItem
                    text="Delete"
                    onClick={(e) => handleDeleteFlashCard(e, item._id)}
                  />
                  <MenuItem
                    text="Edit"
                    onClick={() => {
                      setOpenModal(true);

                      setFlashCardId(item._id);
                      setFlashCardName(item.name);
                      setFrontCard(item.frontCard);
                      setFrontCardImageUrl(item.frontCardImgUrl);
                      setBackCard(item.backCard);
                      setBackCardImage(item.backCardImgUrl);
                    }}
                  />
                </DropdownMenu>
              </div>
              {/* <Link
                to={`/${user}/study/flash-cards/${courseName}/${courseId}/${item._id}`}
              >
              </Link> */}
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '150px',
                }}
                onClick={() => {
                  setOpenModalFlashCard(true);
                  setIsRotated(false);
                  setFlashCardName(item.name);
                  setFrontCard(item.frontCard);
                  setFrontCardImageUrl(item.frontCardImgUrl);
                  setBackCard(item.backCard);
                  setBackCardImage(item.backCardImgUrl);
                }}
              >
                {item.name}
              </div>
            </div>
          ))}
          <Modal
            isOpen={openModalFalshCard}
            // onRequestClose={() => setOpenModalFlashCard(false)}
            className={`card ${isRotated ? 'rotated' : ''}`}
            style={{
              overlay: {
                zIndex: 1005,
                backgroundColor: 'rgba(237, 235, 234, 0.8)',
              },
            }}
          >
            <div className="front">
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  backgroundColor: 'lightpink',
                  width: '90%',
                  alignItems: 'center',
                }}
              >
                <div>Front</div>
                <FontAwesomeIcon
                  icon="times"
                  size="2x"
                  onClick={() => setOpenModalFlashCard(false)}
                />
              </div>
              <div
                style={{
                  backgroundColor: 'red',
                  height: '100px',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}
              >
                <h2>{flashCardName}</h2>
                <h3>{frontCard}</h3>
              </div>
              <img
                style={{
                  backgroundColor: 'blue',
                  width: '200px',
                }}
                src={frontCardImageUrl}
                alt={flashCardName}
              />
              <FontAwesomeIcon icon="redo" onClick={onRotate} />
            </div>
            <div className="back">
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  backgroundColor: 'lightpink',
                  width: '90%',
                  alignItems: 'center',
                }}
              >
                <div>Back</div>
                <FontAwesomeIcon
                  icon="times"
                  size="2x"
                  onClick={() => setOpenModalFlashCard(false)}
                />
              </div>
              <div
                style={{
                  backgroundColor: 'red',
                  height: '100px',
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}
              >
                <h3>{backCard}</h3>
              </div>
              <img
                style={{
                  backgroundColor: 'blue',
                  width: '200px',
                }}
                src={backCardImage}
                alt={flashCardName}
              />
              <FontAwesomeIcon icon="redo" onClick={onRotate} />
            </div>
          </Modal>
        </div>
      </div>
    </Container>
  );
};

export default FlashCards;
