import React, { useEffect, useState } from 'react';

import Container from '../../../components/Container';
import StudyModule from '../../../components/StudyModulesContainer';
import AddItemContainer from '../../../components/AddItemContainer';
import CustomModal from '../../../components/Modal';
import TitleAndIconClose from '../../../components/Modal/titleAndIconClose';
import TextAndComponentContainer from '../../../components/Modal/textAndComponentContainer';
import Input from '../../../components/Input';
import SubmitBottomButtons from '../../../components/Modal/submitBottomButtons';
import SwitchSelector from '../../../components/SwitchSelector';
import Button from '../../../components/Button';
import TextLink from '../../../components/TextLink';

import { CountdownCircleTimer } from 'react-countdown-circle-timer';

import { courseColors } from '../../../utils';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import styles from './Pomodoros.module.css';

const Pomodoro = () => {
  const [openModal, setOpenModal] = useState(false);
  const [openPomodoroModal, setOpenPomodoroModal] = useState(true);

  const [pomodoroName, setPomodoroName] = useState('');
  const [pomodoroColorPosition, setPomodoroColorPosition] = useState(0);

  const [start, setStart] = useState(false);
  const [pause, setPause] = useState(false);
  const [pomodoroOver, setPomodoroOver] = useState(false);
  const [key, setKey] = useState(0);

  const pomodoroModalView = () => {
    const children = (remainingTime) => {
      const minutes = Math.floor(remainingTime / 60);
      const seconds = remainingTime % 60;

      return `${minutes}:${seconds}`;
    };

    return (
      <CustomModal
        open={openPomodoroModal}
        customHeight="420px"
        customTop="20%"
        overlayClick={(value) => setOpenPomodoroModal(value)}
        content={
          <div
            style={{
              height: '100%',
              width: '100%',
              // backgroundColor: 'orange',
            }}
          >
            <TitleAndIconClose
              modalTitle="Pomodoro"
              closeModal={(value) => setOpenPomodoroModal(value)}
            />
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'space-around',
                // backgroundColor: 'red',
                height: '75%',
              }}
            >
              <CountdownCircleTimer
                isPlaying={start ? (pause ? false : true) : false}
                size={270}
                key={key}
                onComplete={() => {
                  // do your stuff here
                  setPomodoroOver(true);
                  // handleAnimation();
                  // return [true, 1500]; // repeat animation in 1.5 seconds
                }}
                duration={10}
                colors={[['#169587'], ['#A6CA63']]}
              >
                {({ remainingTime, animatedColor }) => (
                  <text
                    style={{
                      fontSize: 40,
                      // fontFamily: Typography.RoutineProperties,
                      color: 'black',
                    }}
                  >
                    {pomodoroOver ? 'Finish' : children(remainingTime)}
                  </text>
                )}
              </CountdownCircleTimer>
            </div>
            <SubmitBottomButtons
              cancelFunction={() => {
                setStart(false);
                setPause(false);
                setPomodoroOver(false);
                setKey((prevKey) => prevKey + 1);
                // setPause(true);
              }}
              leftButtonText="Reiniciar"
              submitFunction={() => {
                start ? setPause(!pause) : setStart(true);
              }}
              submitButtonText={
                start ? (pause ? 'Reanudar' : 'Pausa') : 'Start'
              }
              submitBg="lightblue"
            />
          </div>
        }
      />
    );
  };

  return (
    <Container navTitle="Study - Pomodoro" returnScreen="/study">
      <div className={styles.container}>
        <StudyModule
          color1="#169587"
          color2="#A6CA63"
          fixed={true}
          backgroundFigures={
            <>
              <div
                style={{
                  position: 'absolute',
                  width: '300px',
                  height: '300px',
                  backgroundImage:
                    'linear-gradient(to bottom right, #A6CA63, #169587 70%)',
                  borderRadius: '1000px',
                  top: '60px',
                  left: '-40px',
                  transform: 'rotate(20deg)',
                }}
                className={styles.backgroundFigure}
              />
            </>
          }
          leftContentTitle="Pomodoro"
          leftContentDescription="Intervalos de estudio para qur"
          rightContentTop={
            <>
              <FontAwesomeIcon
                color="white"
                icon="bell"
                size="3x"
                style={{
                  transform: 'rotate(-20deg)',
                  position: 'absolute',
                  top: '50px',
                  left: '960px',
                }}
              />
              <FontAwesomeIcon
                color="white"
                icon="redo"
                size="3x"
                style={{
                  transform: 'rotate(20deg)',
                  position: 'absolute',
                  top: '110px',
                  left: '920px',
                }}
              />
            </>
          }
        />
        {/* <CoursesList goPage="notifications-study" /> */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            // backgroundColor: 'red',
            margin: '25px',
          }}
        >
          <AddItemContainer
            itemAreaText="Pomodoros"
            onPressFunction={() => setOpenModal(true)}
            buttonText="New Pomodoro"
          />
          <CustomModal
            open={openModal}
            customHeight="420px"
            customTop="20%"
            overlayClick={(value) => setOpenModal(value)}
            content={
              <div
                style={{
                  // backgroundColor: 'steelblue',
                  height: '100%',
                }}
              >
                <TitleAndIconClose
                  modalTitle="Create New Pomodoro"
                  closeModal={(value) => setOpenModal(value)}
                />
                <div style={{ paddingLeft: 25, paddingRight: 25 }}>
                  <TextAndComponentContainer>
                    <text>Name</text>
                    <Input
                      inputValue={pomodoroName}
                      examplePlaceHolder="Ex; For Math"
                      inputValueFunction={(e) =>
                        setPomodoroName(e.target.value)
                      }
                      inputType="email"
                    />
                  </TextAndComponentContainer>

                  <TextAndComponentContainer>
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                      }}
                    >
                      <text>Concentration Time</text>
                      <text style={{ fontSize: 8 }}>min</text>
                    </div>

                    <Button
                      styleBtn={{
                        width: 335,
                        height: 35,
                        borderRadius: 10,
                        paddingLeft: 20,
                      }}
                      content={
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}
                        >
                          <text style={{ marginRight: 8 }}>
                            Select Concentration Time
                          </text>
                          {/* <FontAwesomeIcon size="xs" icon="clock" /> */}
                        </div>
                      }
                    />
                  </TextAndComponentContainer>

                  <TextAndComponentContainer>
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                      }}
                    >
                      <text>Break Time</text>
                      <text style={{ fontSize: 8 }}>min</text>
                    </div>

                    <Button
                      styleBtn={{
                        width: 335,
                        height: 35,
                        borderRadius: 10,
                        paddingLeft: 20,
                      }}
                      content={
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}
                        >
                          <text style={{ marginRight: 8 }}>
                            Select Break Time
                          </text>
                          {/* <FontAwesomeIcon size="xs" icon="filter" /> */}
                        </div>
                      }
                    />
                  </TextAndComponentContainer>

                  <TextAndComponentContainer>
                    <text>Session</text>

                    <Button
                      styleBtn={{
                        width: 335,
                        height: 35,
                        borderRadius: 10,
                        paddingLeft: 20,
                      }}
                      content={
                        <div
                          style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}
                        >
                          <text style={{ marginRight: 8 }}>
                            Select Sessions
                          </text>
                          {/* <FontAwesomeIcon size="xs" icon="code-branch" /> */}
                        </div>
                      }
                    />
                  </TextAndComponentContainer>

                  <TextAndComponentContainer>
                    <text>Color</text>
                    <SwitchSelector
                      customBorder="8px"
                      scroll={true}
                      content={courseColors.map((item) =>
                        item.position === pomodoroColorPosition ? (
                          <div
                            onClick={() =>
                              setPomodoroColorPosition(item.position)
                            }
                            style={{
                              backgroundImage: `linear-gradient(to bottom right, ${
                                courseColors[item.position].color1
                              }, ${courseColors[item.position].color2} 70%)`,
                              display: 'flex',
                              alignItems: 'center',
                              paddingLeft: '39px',
                              paddingRight: '39px',
                              marginLeft: 2,
                              marginRight: 2,
                              paddingTop: '13px',
                              paddingBottom: '14px',
                              borderRadius: '7px',
                              borderStyle: 'solid',
                              borderWidth: 3,
                              borderColor: 'black',
                            }}
                          />
                        ) : (
                          <div
                            onClick={() =>
                              setPomodoroColorPosition(item.position)
                            }
                            style={{
                              backgroundImage: `linear-gradient(to bottom right, ${
                                courseColors[item.position].color1
                              }, ${courseColors[item.position].color2} 70%)`,
                              display: 'flex',
                              alignItems: 'center',
                              paddingLeft: '39px',
                              paddingRight: '39px',
                              marginLeft: 2,
                              marginRight: 2,
                              paddingTop: '16px',
                              paddingBottom: '15px',
                              borderRadius: '7px',
                            }}
                          />
                        )
                      )}
                    />
                  </TextAndComponentContainer>

                  <SubmitBottomButtons
                    cancelFunction={() => setOpenModal(false)}
                    submitFunction={(e) => {}}
                    submitButtonText="Crear"
                    submitBg="lightblue"
                  />
                </div>
              </div>
            }
          />
          {pomodoroModalView()}
        </div>
      </div>
    </Container>
  );
};

export default Pomodoro;
