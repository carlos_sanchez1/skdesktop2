import React, { useEffect, useState } from 'react';

import Container from '../../../components/Container';
import StudyModule from '../../../components/StudyModulesContainer';
import AddItemContainer from '../../../components/AddItemContainer';
import CustomModal from '../../../components/Modal';
import TitleAndIconClose from '../../../components/Modal/titleAndIconClose';
import TextAndComponentContainer from '../../../components/Modal/textAndComponentContainer';
import Input from '../../../components/Input';
import SubmitBottomButtons from '../../../components/Modal/submitBottomButtons';
import SwitchSelector from '../../../components/SwitchSelector';
import Button from '../../../components/Button';
import TextLink from '../../../components/TextLink';

import { courseColors } from '../../../utils';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import styles from './Exams.module.css';

const Exams = () => {
  const [openModal, setOpenModal] = useState(false);

  const [examName, setExamName] = useState('');
  const [pomodoroColorPosition, setPomodoroColorPosition] = useState(0);

  return (
    <Container navTitle="Study - Pomodoro" returnScreen="/study">
      <div className={styles.container}>
        <StudyModule
          color1="#791BF4"
          color2="#3880EC"
          fixed={true}
          backgroundFigures={
            <>
              <div
                style={{
                  position: 'absolute',
                  width: '300px',
                  height: '300px',
                  backgroundImage:
                    'linear-gradient(to bottom right, #3880EC, #791BF4 70%)',
                  borderRadius: '0px',
                  top: '230px',
                  left: '-130px',
                  transform: 'rotate(70deg)',
                }}
                className={styles.backgroundFigure}
              />
              <div
                style={{
                  position: 'absolute',
                  width: '300px',
                  height: '300px',
                  backgroundImage:
                    'linear-gradient(to bottom right, #3880EC, #791BF4 70%)',
                  borderRadius: '0px',
                  top: '-120px',
                  left: '290px',
                  transform: 'rotate(-60deg)',
                }}
                className={styles.backgroundFigure}
              />
            </>
          }
          leftContentTitle="Pomodoro"
          leftContentDescription="Intervalos de estudio para qur"
          rightContentTop={
            <>
              <FontAwesomeIcon
                color="white"
                icon="bell"
                size="3x"
                style={{
                  transform: 'rotate(-20deg)',
                  position: 'absolute',
                  top: '50px',
                  left: '960px',
                }}
              />
              <FontAwesomeIcon
                color="white"
                icon="redo"
                size="3x"
                style={{
                  transform: 'rotate(20deg)',
                  position: 'absolute',
                  top: '110px',
                  left: '920px',
                }}
              />
            </>
          }
        />
        {/* <CoursesList goPage="notifications-study" /> */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            // backgroundColor: 'red',
            margin: '25px',
          }}
        >
          <AddItemContainer
            itemAreaText="Exams"
            onPressFunction={() => setOpenModal(true)}
            buttonText="New Exam"
          />
          <CustomModal
            open={openModal}
            customHeight="250px"
            customTop="20%"
            overlayClick={(value) => setOpenModal(value)}
            content={
              <div
                style={{
                  // backgroundColor: 'steelblue',
                  height: '100%',
                }}
              >
                <TitleAndIconClose
                  modalTitle="Create New Exam"
                  closeModal={(value) => setOpenModal(value)}
                />
                <div style={{ paddingLeft: 25, paddingRight: 25 }}>
                  <TextAndComponentContainer>
                    <text>Name</text>
                    <Input
                      inputValue={examName}
                      examplePlaceHolder="Ex; Math"
                      inputValueFunction={(e) => setExamName(e.target.value)}
                      inputType="email"
                    />
                  </TextAndComponentContainer>

                  <TextAndComponentContainer>
                    <text>Color</text>
                    <SwitchSelector
                      customBorder="8px"
                      scroll={true}
                      content={courseColors.map((item) =>
                        item.position === pomodoroColorPosition ? (
                          <div
                            onClick={() =>
                              setPomodoroColorPosition(item.position)
                            }
                            style={{
                              backgroundImage: `linear-gradient(to bottom right, ${
                                courseColors[item.position].color1
                              }, ${courseColors[item.position].color2} 70%)`,
                              display: 'flex',
                              alignItems: 'center',
                              paddingLeft: '39px',
                              paddingRight: '39px',
                              marginLeft: 2,
                              marginRight: 2,
                              paddingTop: '13px',
                              paddingBottom: '14px',
                              borderRadius: '7px',
                              borderStyle: 'solid',
                              borderWidth: 3,
                              borderColor: 'black',
                            }}
                          />
                        ) : (
                          <div
                            onClick={() =>
                              setPomodoroColorPosition(item.position)
                            }
                            style={{
                              backgroundImage: `linear-gradient(to bottom right, ${
                                courseColors[item.position].color1
                              }, ${courseColors[item.position].color2} 70%)`,
                              display: 'flex',
                              alignItems: 'center',
                              paddingLeft: '39px',
                              paddingRight: '39px',
                              marginLeft: 2,
                              marginRight: 2,
                              paddingTop: '16px',
                              paddingBottom: '15px',
                              borderRadius: '7px',
                            }}
                          />
                        )
                      )}
                    />
                  </TextAndComponentContainer>

                  <SubmitBottomButtons
                    cancelFunction={() => setOpenModal(false)}
                    submitFunction={(e) => {}}
                    submitButtonText="Crear"
                    submitBg="lightblue"
                  />
                </div>
              </div>
            }
          />
        </div>
      </div>
    </Container>
  );
};

export default Exams;
